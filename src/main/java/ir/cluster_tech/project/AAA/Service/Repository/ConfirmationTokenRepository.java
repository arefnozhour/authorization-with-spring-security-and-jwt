package ir.cluster_tech.project.AAA.Service.Repository;


import ir.cluster_tech.project.AAA.Entity.ConfirmationToken;
import org.springframework.data.repository.CrudRepository;

public interface ConfirmationTokenRepository extends CrudRepository<ConfirmationToken, String> {
	ConfirmationToken findByConfirmationToken(String confirmationToken);
}
