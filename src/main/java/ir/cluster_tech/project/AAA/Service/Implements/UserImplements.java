package ir.cluster_tech.project.AAA.Service.Implements;
import ir.cluster_tech.project.AAA.Common.PassEncoding;
import ir.cluster_tech.project.AAA.Common.RException;
import ir.cluster_tech.project.AAA.Common.Roles;
import ir.cluster_tech.project.AAA.Entity.ConfirmationToken;
import ir.cluster_tech.project.AAA.Entity.ResetPasswordToken;
import ir.cluster_tech.project.AAA.Entity.User;
import ir.cluster_tech.project.AAA.Service.Interface.UserInterface;
import ir.cluster_tech.project.AAA.Service.Repository.ConfirmationTokenRepository;
import ir.cluster_tech.project.AAA.Service.Repository.UserRepository;
import ir.cluster_tech.project.Common.EmailDomain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;


@Service
public class UserImplements implements UserInterface {

	private final String email="your email";


	@Qualifier("userRepository")
	@Autowired
	private UserRepository usrep;

	@Autowired
	private ConfirmationTokenRepository ConfirmationTokenRepository;

	@Autowired
	private EmailSenderService emailSenderService;


	@Autowired
	private ir.cluster_tech.project.AAA.Service.Repository.ResetPasswordTokenRepository ResetPasswordTokenRepository;


	@Override
	public User register(User user) throws RException {
		if (this.usrep.findByEmail(user.getEmail())!=null)
			throw new RException(404);
		if(this.usrep.findByUsername(user.getUsername())!=null)
			throw new RException(405);

		user.setIsactive(false);
		user.setPassword(PassEncoding.getInstance().passwordEncoder.encode(user.getPassword()));
		user.setRole(Roles.ROLE_USER.getValue());
		usrep.save(user);
		ConfirmationToken confirmationToken = new ConfirmationToken(user);

		ConfirmationTokenRepository.save(confirmationToken);

		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setTo(user.getEmail());
		mailMessage.setSubject("Complete Registration!");
		mailMessage.setFrom(email);
		mailMessage.setText("To confirm your account, please click here : "+
				EmailDomain.domain+"AAA/confirm-account?token=" + confirmationToken.getConfirmationToken());
		emailSenderService.sendEmail(mailMessage);
		return user;
	}

	@Override
	public User emailconfirm(String confirmationToken) throws RException
	{
		ConfirmationToken token = ConfirmationTokenRepository.findByConfirmationToken(confirmationToken);
		if (token != null)
		{
			User person = usrep.findByEmail(token.getUser().getEmail());
			person.setIsactive(true);
			usrep.save(person);
			return person;
		}
		////////////token is invalid
		throw new RException(201);
	}

	@Override
	public User forgetpass(String email) throws RException
	{
		User person = usrep.findByEmail(email);
		if(person==null)
			throw new RException(201);
		ResetPasswordToken resetpassToken = new ResetPasswordToken(person);
		resetpassToken.setIsused(false);
		ResetPasswordTokenRepository.save(resetpassToken);

		SimpleMailMessage mailMessage = new SimpleMailMessage();
		mailMessage.setTo(person.getEmail());
		mailMessage.setSubject("GetNewPass");
		mailMessage.setFrom(email);
		mailMessage.setText("To get new pass Click here " + EmailDomain.domain+"AAA/set-pass?token="
				+ resetpassToken.getResetpassToken());
		emailSenderService.sendEmail(mailMessage);
		return person;
	}


	@Override
	public User resetpassword(User user,String pass) throws RException
	{

        user.setPassword(PassEncoding.getInstance().passwordEncoder.encode(pass));
        user=this.usrep.save(user);
        return user;
	}
}
