package ir.cluster_tech.project.AAA.Service.Repository;

import ir.cluster_tech.project.AAA.Entity.ResetPasswordToken;
import org.springframework.data.repository.CrudRepository;


public interface ResetPasswordTokenRepository extends CrudRepository<ResetPasswordToken, String> {
	ResetPasswordToken findByResetpassToken(String resetpassToken);

}
