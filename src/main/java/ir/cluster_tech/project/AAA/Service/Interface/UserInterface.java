package ir.cluster_tech.project.AAA.Service.Interface;


import ir.cluster_tech.project.AAA.Common.RException;
import ir.cluster_tech.project.AAA.Entity.User;

import javax.transaction.Transactional;

public interface UserInterface {



    @Transactional
    User register(User user)  throws RException;


    @Transactional
    User emailconfirm(String email) throws RException;


    @Transactional
    User forgetpass(String email) throws RException;


    @Transactional
    User resetpassword(User user, String pass) throws RException;
}
