package ir.cluster_tech.project.AAA.Service.Repository;


import ir.cluster_tech.project.AAA.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {

	User findByUsername(String username);

	User findByEmail(String email);


}