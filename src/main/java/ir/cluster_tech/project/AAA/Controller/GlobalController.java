package ir.cluster_tech.project.AAA.Controller;

import ir.cluster_tech.project.AAA.Entity.User;
import ir.cluster_tech.project.AAA.Service.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
@Controller
public class GlobalController {

	@Autowired
	private UserRepository userService;

	private User loginUser;

	public User getLoginUser() {
		if (loginUser == null) {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			loginUser = userService.findByUsername(auth.getName());
		}
		return loginUser;
	}

}
