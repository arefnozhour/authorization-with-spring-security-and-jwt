package ir.cluster_tech.project.AAA.Controller.model;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class register_model {
    private String username;

    private String password;

    private String email;
}
