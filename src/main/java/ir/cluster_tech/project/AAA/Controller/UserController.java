package ir.cluster_tech.project.AAA.Controller;

import ir.cluster_tech.project.AAA.Common.RException;
import ir.cluster_tech.project.AAA.Controller.model.register_model;
import ir.cluster_tech.project.AAA.Entity.ResetPasswordToken;
import ir.cluster_tech.project.AAA.Entity.User;
import ir.cluster_tech.project.AAA.Service.Implements.EmailSenderService;
import ir.cluster_tech.project.AAA.Service.Interface.UserInterface;
import ir.cluster_tech.project.AAA.Service.Repository.ConfirmationTokenRepository;
import ir.cluster_tech.project.AAA.Service.Repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@Controller
@RequestMapping("/AAA")
public class UserController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private GlobalController globalController;

	@Autowired
	private ConfirmationTokenRepository ConfirmationTokenRepository;

	@Autowired
	private EmailSenderService emailSenderService;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private UserInterface userImple;



    @Autowired
    private ir.cluster_tech.project.AAA.Service.Repository.ResetPasswordTokenRepository ResetPasswordTokenRepository;




	@RequestMapping(value = { "/logout" }, method = RequestMethod.GET)
	public ResponseEntity<Integer> logout(HttpServletRequest request) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if (auth!=null)
		{
			try {
				request.logout();
				SecurityContextHolder.clearContext();
				HttpSession session = request.getSession(false);
				if (session != null) {
					session.invalidate();
					return new ResponseEntity<Integer>(200, HttpStatus.OK);
				}
			} catch (ServletException e) {
				return new ResponseEntity<Integer>(404,HttpStatus.OK);
			}
		}
		return new ResponseEntity<Integer>(200,HttpStatus.OK);
	}



    @RequestMapping(value = { "/home" }, method = RequestMethod.GET)
    public ResponseEntity<String> ll(Authentication authentication) {
        return new ResponseEntity<String>("hiii",HttpStatus.OK);
    }
	@RequestMapping(value = { "/register" }, method = RequestMethod.POST)
	public ResponseEntity<Integer> register(@RequestBody register_model rg) {
		User user=new User(rg.getUsername(),rg.getPassword(),rg.getEmail());
		try {
			this.userImple.register(user);
			return new ResponseEntity<Integer>(200, HttpStatus.OK);
		} catch (RException e) {
			return new ResponseEntity<Integer>(e.getStatus(), HttpStatus.OK);
		}
	}

	@SuppressWarnings("unused")
	@RequestMapping(value = "/confirm-account", method = RequestMethod.POST)
	public ResponseEntity<Integer> confirmUserAccount(@RequestParam("token") String confirmationToken) {
		try {
			this.userImple.emailconfirm(confirmationToken);
			return new ResponseEntity<Integer>(200,HttpStatus.OK);
		} catch (RException e) {
			return new ResponseEntity<Integer>(e.getStatus(),HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/forgetpassword", method = RequestMethod.POST )
	public ResponseEntity<Integer> forgetpassword(@RequestParam("email") String email ) {
		try {
			this.userImple.forgetpass(email);
			return new ResponseEntity<Integer>(200,HttpStatus.OK);
		} catch (RException e) {
			return new ResponseEntity<Integer>(e.getStatus(),HttpStatus.OK);
		}
	}

	@RequestMapping(value = "/set-pas", method = RequestMethod.POST )
	public String Resetpass(@RequestParam("token") String resetpassToken, HttpSession session) {
		ResetPasswordToken token = ResetPasswordTokenRepository.findByResetpassToken(resetpassToken);
		session.setAttribute("user",token.getUser());
        if (token==null)
            return "redirect:/404";
        return "redirect:/restpass";
	}

}
