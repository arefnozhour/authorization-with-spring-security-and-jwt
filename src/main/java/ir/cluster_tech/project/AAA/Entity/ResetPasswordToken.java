package ir.cluster_tech.project.AAA.Entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@ToString
@Entity
@Table(name = "resetpasswordtoken", schema = "rewandity")
public class ResetPasswordToken {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long tokenid;

	@Column(name = "resetpasstoken", unique = true)
	private String resetpassToken;

	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@Column(name = "isused")
	private boolean isused;

	@ManyToOne
	@JoinColumn(nullable = false, name = "userid")
	private User user;

	public ResetPasswordToken(User user) {
		this.user = user;
		createdDate = new Date();
		resetpassToken = UUID.randomUUID().toString();

	}

	public ResetPasswordToken() {

	}

}
