package ir.cluster_tech.project.AAA.Entity;

import javax.persistence.*;
import java.util.Objects;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Getter
@Setter
@ToString
@Entity
@Table(name = "user", schema = "rewandity")
@Component
@Scope("session")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "username", unique = true)
	private String username;

	@Column(name = "password")
	private String password;


	@Column(name = "email")
	private String email;

	@Column(name = "role")
	private int role;

	@Column(name = "isactive")
	private Boolean isactive;

	public User() {
	}

	public User(String username, String password, String email) {
		this.setUsername(username);
		this.setPassword(password);
		this.setEmail(email);

	}
}
