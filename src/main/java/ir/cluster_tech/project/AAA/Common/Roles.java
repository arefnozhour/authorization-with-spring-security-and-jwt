package ir.cluster_tech.project.AAA.Common;

public enum Roles {
	ROLE_ADMIN(1), ROLE_USER(2), ROLE_SUPERADMIN(3), ROLE_COACH(4);
	private int value;

	Roles(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
 